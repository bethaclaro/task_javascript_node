# Getting Started

## Install dependencies
```
npm install
```

## Execute
Requires a file. 
```
node app.js input.json
```

## Run tests

```
npm test
```