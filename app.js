/* eslint-disable no-console */

const fs = require('fs');
const service = require('./app/service');
const compute = require('./app/compute');

console.clear();

const TYPE_CASH_IN = 'cash_in';
const TYPE_CASH_OUT = 'cash_out';
const USER_TYPE_NATURAL = 'natural';

async function app() {
  const file = process.argv[2];

  if (file) {
    console.log('processing file...');

    const getConfigCashIn = await service.getCashIn();
    const getConfigCashOutN = await service.getCashOutNatural();
    const getConfigCashOutJ = await service.getCashOutJuridical();

    fs.readFile(file, (err, data) => {
      if (err) console.log('ERROR reading file');

      const fileContent = JSON.parse(data);

      fileContent.forEach((item) => {
        if (item.type === TYPE_CASH_IN) {
          console.log(compute.cashIn(getConfigCashIn.data, item.operation.amount));
        } else if (item.type === TYPE_CASH_OUT && item.user_type === USER_TYPE_NATURAL) {
          console.log(compute.cashOutN(getConfigCashOutN.data, item));
        } else {
          console.log(compute.cashOutJ(getConfigCashOutJ.data, item.operation.amount));
        }
      });
    });
  } else {
    console.log('No file given');
  }
}

app();
