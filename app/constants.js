const HOSTNAME = 'http://private-anon-f34e888a98-uzduotis.apiary-mock.com';
const PATH_CASH_IN = '/config/cash-in';
const PATH_CASH_OUT_NATURAL = '/config/cash-out/natural';
const PATH_CASH_OUT_JURIDICAL = '/config/cash-out/juridical';

module.exports = {
  HOSTNAME,
  PATH_CASH_IN,
  PATH_CASH_OUT_JURIDICAL,
  PATH_CASH_OUT_NATURAL,
};

// export const HOSTNAME = 'http://private-38e18c-uzduotis.apiary-mock.com';
// export const PATH_CASH_IN = '/config/cash-in';
// export const PATH_CASH_OUT_NATURAL = '/config/cash-out/natural';
// export const PATH_CASH_OUT_JURIDICAL = 'config/cash-out/juridical';
