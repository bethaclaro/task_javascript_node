/* eslint-disable no-console */
const axios = require('axios');
const constants = require('../constants');

const getCashIn = () => {
  let res;
  try {
    res = axios.get(`${constants.HOSTNAME}${constants.PATH_CASH_IN}`);
  } catch (e) {
    console.error(e);
  }
  return res;
};

const getCashOutNatural = () => {
  let res;
  try {
    res = axios.get(`${constants.HOSTNAME}${constants.PATH_CASH_OUT_NATURAL}`);
  } catch (e) {
    console.error(e);
  }
  return res;
};

const getCashOutJuridical = () => {
  let res;
  try {
    res = axios.get(`${constants.HOSTNAME}${constants.PATH_CASH_OUT_JURIDICAL}`);
  } catch (e) {
    console.error(e);
  }
  return res;
};

module.exports = {
  getCashIn,
  getCashOutNatural,
  getCashOutJuridical,
};
