/* eslint-disable no-console */
const moment = require('moment');

moment.updateLocale('en', {
  week: {
    dow: 1, // Monday is the first day of the week.
  },
});

const arrUsers = [];
const findUser = (id, week) => arrUsers.find((item) => item.user_id === id
  && item.week === week);
const findUserIdx = (id, week) => arrUsers.findIndex((item) => item.user_id === id
  && item.week === week);

const cashIn = (config, amount) => {
  const comm = amount * (config.percents / 100);
  if (comm <= config.max.amount) return comm.toFixed(2);
  return (config.max.amount).toFixed(2);
};

const cashOutN = (config, record) => {
  const week = moment(record.date).week();
  const found = findUser(record.user_id, week);

  if (!found) {
    const temp = { user_id: record.user_id, cashouts: record.operation.amount, week };
    arrUsers.push(temp);
    if (record.operation.amount > config.week_limit.amount) {
      const newcomm = (
        record.operation.amount - config.week_limit.amount) * (config.percents / 100);
      return newcomm.toFixed(2);
    }
    return (0).toFixed(2);
  }

  if (found) {
    const totalC = found.cashouts + record.operation.amount;
    const temp = { ...found, cashouts: totalC };
    const idx = findUserIdx(record.user_id, week);
    arrUsers[idx] = temp;
    if (totalC >= config.week_limit.amount) {
      const newcomm = record.operation.amount * (config.percents / 100);
      return newcomm.toFixed(2);
    }
    return (0).toFixed(2);
  }

  return (0).toFixed(2);
};

const cashOutJ = (config, amount) => {
  const comm = amount * (config.percents / 100);
  if (amount > config.min.amount) return comm.toFixed(2);
  return (0).toFixed(2);
};

module.exports = {
  cashIn, cashOutJ, cashOutN,
};
