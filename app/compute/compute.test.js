const compute = require('./index');

const cashInConfig = { percents: 0.03, max: { amount: 5, currency: 'EUR' } };
const cashOutNConfig = { percents: 0.3, week_limit: { amount: 1000, currency: 'EUR' } };
const cashOutJConfig = { percents: 0.3, min: { amount: 0.5, currency: 'EUR' } };

describe('TEST - Cash_in computation', () => {
  test('Cash In, 0.03, 200 - should be 0.06', () => {
    expect(compute.cashIn(cashInConfig, 200.00)).toBe('0.06');
  });

  test('Cash In, 0.03, 300 - should be 0.09', () => {
    expect(compute.cashIn(cashInConfig, 300.00)).toBe('0.09');
  });
});

describe('TEST - Cash_out computation for Natural', () => {
  test('Cash Out, 0.3, limit 1000, amount 30000 - should be 87.00', () => {
    const record = {
      date: '2016-01-06', user_id: 1, user_type: 'natural', type: 'cash_out', operation: { amount: 30000, currency: 'EUR' },
    };
    expect(compute.cashOutN(cashOutNConfig, record)).toBe('87.00');
  });

  test('Cash Out, 0.3, limit 1000, amount 10000, same user - should be 30.00', () => {
    const record = {
      date: '2016-01-06', user_id: 1, user_type: 'natural', type: 'cash_out', operation: { amount: 10000, currency: 'EUR' },
    };
    expect(compute.cashOutN(cashOutNConfig, record)).toBe('30.00');
  });

  test('Cash Out, 0.3, limit 1000, amount 10000, diff user - should be 27.00', () => {
    const record = {
      date: '2016-01-06', user_id: 2, user_type: 'natural', type: 'cash_out', operation: { amount: 10000, currency: 'EUR' },
    };
    expect(compute.cashOutN(cashOutNConfig, record)).toBe('27.00');
  });

  test('Cash Out, 0.3, limit 1000, amount 1000, diff user - should be 0.00', () => {
    const record = {
      date: '2016-01-06', user_id: 3, user_type: 'natural', type: 'cash_out', operation: { amount: 1000, currency: 'EUR' },
    };
    expect(compute.cashOutN(cashOutNConfig, record)).toBe('0.00');
  });
});

describe('TEST - Cash_out computation for Juridical', () => {
  test('Cash Out, 0.3, min 0.5, amount 300.00 - should be 0.90', () => {
    expect(compute.cashOutJ(cashOutJConfig, 300.00)).toBe('0.90');
  });

  test('Cash Out, 0.3, min 0.5, amount 0.30 - should be 0.00', () => {
    expect(compute.cashOutJ(cashOutJConfig, 0.30)).toBe('0.00');
  });
});
