/* eslint-disable no-continue */
const generateBuzz = (num1n, num1s, num2n, num2s, condition1, condition2) => {
  const arr = [];

  for (let y = 1; y <= 100; y++) {
    // const condition1 = y % num1n === 0;
    // const condition2 = y % num2n === 0;

    if (condition1(y) && condition2(y)) {
      arr.push(`${num1s}${num2s}`);
      continue;
    }

    if (condition1(y)) {
      arr.push(num1s);
      continue;
    }

    if (condition2(y)) {
      arr.push(num2s);
      continue;
    }

    arr.push(y);
  }

  return arr.join(' ');
};

console.log(generateBuzz(3, 'Fizz', 5, 'Buzz', (y) => y % 3 === 0, (y) => y % 5 === 0));
// console.log(generateBuzz(7, 'Bar', 11, 'Baz', (y) => y % 7 === 0, (y) => y % 11 === 0));
// console.log(generateBuzz(3, 'Fizz', 5, 'Buzz', (y) => y >= 3 && y <= 6, (y) => (y % 10) === 5));
